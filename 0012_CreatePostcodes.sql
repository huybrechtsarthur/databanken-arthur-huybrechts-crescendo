USE ModernWays;

DROP TABLE IF EXISTS `Postcodes`;
CREATE TABLE Postcodes(
    Code CHAR(4),
    Plaats NVARCHAR(120),
    Localite NVARCHAR(120),
    Provincie NVARCHAR(120),
    Province NVARCHAR(120)
);