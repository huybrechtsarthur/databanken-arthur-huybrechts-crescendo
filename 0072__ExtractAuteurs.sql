use ModernWays;

-- stap 1 --
DROP TABLE IF EXISTS `Auteurs`;

CREATE TABLE `Auteurs` (
`Id` int(1) NOT NULL AUTO_INCREMENT PRIMARY KEY,
`Voornaam` varchar(255),
`Familienaam` varchar(255)
);

-- stap 2 --
insert into Auteurs(Voornaam,Familienaam)
select distinct Voornaam,Familienaam
from Boeken;
 
 -- stap 3 --
alter table Boeken
add column Auteurs_Id int,
add FOREIGN KEY fk_Boeken_Auteurs(Auteurs_Id)
REFERENCES Auteurs(Id);

-- stap 4 --
update Boeken, Auteurs
set Auteurs_Id = Auteurs.Id
where ((Auteurs.Voornaam IS NULL AND Boeken.Voornaam IS NULL) OR (Auteurs.Voornaam = Boeken.Voornaam)) AND
((Auteurs.Achternaam IS NULL AND Boeken.Achternaam IS NULL) OR (Auteurs.Achternaam = Boeken.Achternaam));
 
 -- stap 5 --
 alter table Boeken modify Auteurs_Id INT not null;
 
 -- stap 6 --
 alter table Boeken drop Voornaam, drop Familienaam;