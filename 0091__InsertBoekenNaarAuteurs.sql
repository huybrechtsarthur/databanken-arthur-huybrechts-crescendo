use modernways;
insert into BoekenNaarAuteurs(Boeken_Id, Auteurs_Id)
select Boeken.Id, Auteurs.Id
from Boeken
inner join Auteurs where
(Boeken.Titel = "Norwegian Wood" AND Auteurs.Voornaam = "Haruki" AND Auteurs.Familienaam = "Murakami")
OR (Boeken.Titel = "Kafka on the Shore" AND Auteurs.Voornaam = "Haruki" AND Auteurs.Familienaam = "Murakami")
OR (Boeken.Titel = "American Gods" AND Auteurs.Voornaam = "Neil" AND Auteurs.Familienaam = "Gaiman")
OR (Boeken.Titel = "The OCean at the End of the Lane" AND Auteurs.Voornaam = "Neil" AND Auteurs.Familienaam = "Gaiman")
OR (Boeken.Titel = "Pet Sematary" AND Auteurs.Voornaam = "Stephen" AND Auteurs.Familienaam = "King")
OR (Boeken.Titel = "Good Omens" AND Auteurs.Voornaam = "Neil" AND Auteurs.Familienaam = "Gaiman")
OR (Boeken.Titel = "Good Omens" AND Auteurs.Voornaam = "Terry" AND Auteurs.Familienaam = "Pratchett")
OR (Boeken.Titel = "The Talisman" AND Auteurs.Voornaam = "Stephen" AND Auteurs.Familienaam = "King")
OR (Boeken.Titel = "The Talisman" AND Auteurs.Voornaam = "Peter" AND Auteurs.Familienaam = "Straub")
;