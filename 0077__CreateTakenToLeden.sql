use modernways;

drop table if exists `TakenToLeden`;

CREATE TABLE `TakenToLeden` (
	`Leden_Id` int not null,
    `Taken_Id` int not null
);

ALTER TABLE `TakenToLeden`
add FOREIGN KEY fk_TakenToLeden_Taken(Taken_Id)
references Taken(Id);

ALTER TABLE `TakenToLeden`
add FOREIGN KEY fk_TakenToLeden_Leden(Leden_Id)
references Leden(Id);