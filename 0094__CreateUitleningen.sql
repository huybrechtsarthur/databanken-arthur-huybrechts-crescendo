use modernways;

drop table if exists Uitleningen;
create table Uitleningen(
Id int(11) primary key not null auto_increment,
Leden_Id int(11) not null,
Boeken_Id int(11) not null,
Startdatum date,
Einddatum date,
key fk_Uitleningen_Leden (Leden_Id),
key fk_Uitleningen_Boeken (Boeken_Id),
constraint fk_Uitleningen_Leden foreign key (Leden_Id) references Leden(Id),
constraint fk_Uitleningen_Boeken foreign key (Boeken_Id) references Boeken(Id)
);