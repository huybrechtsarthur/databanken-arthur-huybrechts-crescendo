use modernways;
-- Je moet Personen ook vermelden, ook al wordt enkel Games aangepast!
update Games, Personen
set Personen_Id = Personen.Id
where Games.Voornaam = Personen.Voornaam AND
 Games.Familienaam = Personen.Familienaam AND
 Games.Email = Personen.Email;