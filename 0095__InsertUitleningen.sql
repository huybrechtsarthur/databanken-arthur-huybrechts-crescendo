use modernways;

insert into Uitleningen(Leden_Id, Boeken_Id, Startdatum, Einddatum)
select Leden.Id, Boeken.Id,  "2019-02-01", "2019-02-15"
from Leden
inner join Boeken where
(Leden.Voornaam = "Max" AND Boeken.Titel = "Norwegian Wood")
;
insert into Uitleningen(Leden_Id, Boeken_Id, Startdatum, Einddatum)
select Leden.Id, Boeken.Id,  "2019-02-16", "2019-03-02"
from Leden
inner join Boeken where
(Leden.Voornaam = "Bavo" AND Boeken.Titel = "Norwegian Wood")
;
insert into Uitleningen(Leden_Id, Boeken_Id, Startdatum, Einddatum)
select Leden.Id, Boeken.Id,  "2019-02-16", "2019-03-02"
from Leden
inner join Boeken where
(Leden.Voornaam = "Bavo" AND Boeken.Titel = "Pet Sematary")
;
insert into Uitleningen(Leden_Id, Boeken_Id, Startdatum, Einddatum)
select Leden.Id, Boeken.Id,  "2019-05-01", null
from Leden
inner join Boeken where
(Leden.Voornaam = "Yannick" AND Boeken.Titel = "Pet Sematary")
;