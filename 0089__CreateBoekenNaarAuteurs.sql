use modernways;

DROP TABLE IF EXISTS `BoekenNaarAuteurs`;

CREATE TABLE `BoekenNaarAuteurs` (
  `Boeken_Id` int(11) NOT NULL,
  `Auteurs_Id` int(11) NOT NULL,
  KEY `fk_BoekenNaarAuteurs_Boeken` (`Boeken_Id`),
  KEY `fk_BoekenNaarAuteurs_Auteurs` (`Auteurs_Id`),
  CONSTRAINT `fk_BoekenNaarAuteurs_Auteurs` FOREIGN KEY (`Auteurs_Id`) REFERENCES `Auteurs` (`Id`),
  CONSTRAINT `fk_BoekenNaarAuteurs_Boeken` FOREIGN KEY (`Boeken_Id`) REFERENCES `Boeken` (`Id`)
);