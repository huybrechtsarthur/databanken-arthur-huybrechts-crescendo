use modernways;

insert into Boeken (
	Id,
    Voornaam,
    Familienaam,
    Titel,
    Stad,
    Uitgeverij,
    Verschijningsdatum,
    Herdruk,
    Commentaar,
    Categorie
)
values
(1 ,'Samuel', 'Ijsseling', 'Heidegger. Denken en Zijn. Geven en Danken', 'Amsterdagm', '', '2014', '', 'Nog te lezen', 'Filosofie'),
(2, 'Jacob', 'Van Sluis', 'Lees wijzer bij Zijn en Tijd', '', 'Budel', '1998', '', 'Goed boek', 'Filosofie');