use `modernways`;

insert into `Boeken` (Voornaam,Familienaam,Titel,Stad,Uitgeverij,Verschijningsdatum,Herdruk,Commentaar,Categorie)
values
('Evert W.','Beth','Wijsbegeerte der Wiskunde','Antwerpen','Philosophische Biliotheek Uitgeversmij. N.V. Standaard-Boekhandel',1948,'?','Een goed boek','Wiskunde'),
('Pierre','Bonte en Michel Izard','Dictionnaire de l''etnologie et de l''anthropologie','?','PUF',1991,'?','Een goed boek','Anthropologie'),
('Fernand','Braudel','De middellandse zee. Het landschap en de mens','Amsterdam/Antwerpen','Uitgeverij Contanct',1992,'?','Uit het Frans vertaald: La méditerranée. La part du milieu. Parijs: Librairie Armand Colin, 1966','Geschiedenis');

