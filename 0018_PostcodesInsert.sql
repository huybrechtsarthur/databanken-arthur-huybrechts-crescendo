use ModernWays;

insert into Postcodes (
	Code,
    Plaats,
    Localite,
    Provincie,
    Province
)
values
(2800, 'Mechelen', 'Malines', 'Antwerpen', 'Anvers'),
(3000, 'Leuven', 'Louvain', 'Vlaams-Brabant', 'Brabant flamand')