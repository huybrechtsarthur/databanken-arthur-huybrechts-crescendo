use ModernWays;

DROP TABLE IF EXISTS `Personen`;

CREATE TABLE `Personen` (
`Id` int(1) NOT NULL AUTO_INCREMENT PRIMARY KEY,
`Voornaam` varchar(255) NOT NULL,
`Familienaam` varchar(255) NOT NULL,
`Email` varchar(255) NOT NULL
)