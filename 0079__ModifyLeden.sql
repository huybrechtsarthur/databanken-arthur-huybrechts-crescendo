use modernways;

alter table Leden
add column Taken_Id int,
add FOREIGN KEY fk_Leden_Taken(Taken_Id)
references Taken(Id);