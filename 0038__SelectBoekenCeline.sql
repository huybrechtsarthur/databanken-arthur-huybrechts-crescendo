insert into Boeken (
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsdatum)
values
  ('Céline', 'Claus', 'De verwondering', 'Antwerpen', 'Manteau', '1970'),
   ('Celine' ,'Raes', 'Jagen en gejaagd worden', 'Antwerpen', 'De Bezige Bij', '1954'),
   ('CELINE', 'Sarthe', 'Het zijn en het niets', 'Parijs', 'Gallimard', '1943');

select * from Boeken where voornaam = 'Celine';
/* Zou enkel Celine en CELINE moeten selecteren, maar in tegenstelling tot de opdr is de collation hier niet accentgevoelig dus Céline wordt ook geselecteerd*/