USE ModernWays;

ALTER TABLE Boeken CHANGE Voornaam Voornaam VARCHAR(25);
ALTER TABLE Boeken CHANGE Familienaam Familienaam VARCHAR(50);
ALTER TABLE Boeken CHANGE Titel Titel VARCHAR(255);
ALTER TABLE Boeken CHANGE Stad Stad VARCHAR(100);
ALTER TABLE Boeken CHANGE Uitgeverij Uitgeverij VARCHAR(100);
ALTER TABLE Boeken CHANGE Verschijningsjaar Verschijningsdatum CHAR(4);
ALTER TABLE Boeken CHANGE Commentaar Commentaar VARCHAR(500);