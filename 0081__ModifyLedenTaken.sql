use modernways;

alter table Leden
drop foreign key fk_Leden_Taken,
drop column Taken_Id;

alter table Taken
add column Leden_Id int,
add foreign key fk_Taken_Leden(Leden_Id)
references Leden(Id);