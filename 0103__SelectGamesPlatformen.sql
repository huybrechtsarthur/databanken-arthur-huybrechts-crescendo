
use modernways;

SELECT Games.Titel, Platformen.Naam
	FROM Releases
	right JOIN Platformen
	ON Releases.Platformen_Id = Platformen.Id
	left JOIN Games
    ON Releases.Games_Id = Games.Id
    where Games.Id is null
union
SELECT Games.Titel, Platformen.Naam
	FROM Releases
	right JOIN Platformen
	ON Releases.Platformen_Id = Platformen.Id
	right JOIN Games
    ON Releases.Games_Id = Games.Id
    where Releases.Games_Id is null