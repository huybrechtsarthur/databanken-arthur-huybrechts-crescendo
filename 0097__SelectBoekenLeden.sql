SELECT Leden.Voornaam, Boeken.Titel
FROM Uitleningen
     INNER JOIN Leden ON Uitleningen.Leden_Id = Leden.Id
     INNER JOIN Boeken ON Uitleningen.Boeken_Id = Boeken.Id