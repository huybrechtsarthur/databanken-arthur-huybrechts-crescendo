use modernways;

alter table Tweets
add column Users_Id int not null,
add foreign key fk_Tweets_Users(Users_Id)
references Users(Id);