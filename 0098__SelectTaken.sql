use ModernWays;

SELECT Omschrijving
   FROM Taken
   RIGHT JOIN Leden
   ON Taken.Leden_Id = Leden.Id
   WHERE Leden.Id IS NULL
   
/*  kan ook gewoon
SELECT Taken.Omschrijving
FROM Taken
WHERE Taken.Leden_Id IS NULL;
*/