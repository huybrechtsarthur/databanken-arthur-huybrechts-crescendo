use modernways;

drop table if exists `Taken`;

CREATE TABLE `Taken` (
`Omschrijving` VARCHAR(50) NOT NULL,
`Id` int AUTO_INCREMENT PRIMARY KEY);

drop table if exists `Leden`;

CREATE TABLE `Leden` (
`Voornaam` VARCHAR(50) NOT NULL,
`Id` int AUTO_INCREMENT PRIMARY KEY);