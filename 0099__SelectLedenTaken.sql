use ModernWays;

SELECT Voornaam, Omschrijving
   FROM Leden
   LEFT JOIN Taken
   ON Leden.Id = Taken.Leden_Id
   WHERE Taken.Leden_Id IS NULL
UNION
SELECT Voornaam, Omschrijving
   FROM Leden
   RIGHT JOIN Taken
   ON Leden.Id = Taken.Leden_Id
   WHERE Leden.Id IS NULL