use ModernWays;

select Voornaam, Familienaam, Titel, Verschijningsdatum from Boeken
where (Verschijningsdatum between 1900 and 2011) and ((Familienaam like '%B%') or (Familienaam like '%F%') or (Familienaam like '%A%'))
order by Verschijningsdatum;